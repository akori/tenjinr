# tenjinr

Scientific literature recommender system.

[![pipeline status](https://gitlab.com/akori/tenjinr/badges/master/pipeline.svg)](https://gitlab.com/akori/tenjinr/commits/master)

[![coverage report](https://gitlab.com/akori/tenjinr/badges/master/coverage.svg)](https://gitlab.com/akori/tenjinr/commits/master)

## Description

Based on a paper, *tenjinr* will collate a secondary set of publications and 
resources to ease the understanding of the original paper.

There might be already other paper recommending systems, thus our aim is mostly
educational and to explore modern software development tools and practices
together with modern Python and modern technologies.

## How to use it

Install pipenv via pip

`pip install pipenv`

Then, after cloning the repo install the dependencies to use the CLI

`pipenv install`

or

`pipenv install --dev`

to install developement packages too.

To enter to the virtual environment execute

`pipenv shell`

Once inside the virtual environment

`python tenjinr.py`

To execute the tests execute

`pytest`

or

`pipenv run pytest` without the need to activate the environment.

## Technical objectives

The following set of objectives should work as a guideline and not necesary to
be all implemented at the same time. Certainly they will evolve as the project
matures and grows from a _proof of concept_ to eventually a more fully featured 
product

* Proper test
    * Extensive use of parametrization and fixtures of tests
    * Use property based testing
* Async code for all network IO operations
* Structured logging 
* Extensive use of gitlab's and/or third party CI systems

## Python objectives

* Support 3.6 and above only
* Easy use as a cli or as an imported module for interactive use (notebook friendly) 
* Extensive use of type annotations, particularly in broadly used helper functions
* Automatic use of black formatter
* Use of new dataclass or attrs module
* Use pipenv to manage environment and dependencies
* Explore new async capabilities
