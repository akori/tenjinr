import click

from functools import partial

__version__ = "0.0.1"

out = partial(click.secho, bold=True, err=True)
err = partial(click.secho, fg="red", err=True)


@click.command()
@click.option("-v", "--verbose", is_flag=True, help=("Shows extra output"))
@click.option("-f", "--fail", is_flag=True, help=("We want to fail"))
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx: click.Context, verbose: bool, fail: bool) -> None:
    click.echo("This is tenjinr.")
    if verbose:
        out("verbose mode activated!")
    if fail:
        err(f"We failed! 💥!")
        ctx.exit(2)
        # ctx.fail(f"We failed! 💥!")  # without coloring of output just one call


if __name__ == "__main__":
    cli()
