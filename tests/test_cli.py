from click.testing import CliRunner
import pytest  # type: ignore

import tenjinr as t


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_main_fail(runner):
    result = runner.invoke(t.cli, ["--fail"])
    assert result.exit_code == 2
    assert result.output == f"This is tenjinr.\nWe failed! 💥!\n"
